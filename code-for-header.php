<?php


/**
 * Mega Menu
 * Replace the wp_nav_menu function and <nav> tag with the below code to call the mega menu
 * 
 */

?>

<nav id="site-navigation" class="main-navigation">
    <ul id="primary-menu" class="nav-menu">
        <?php
            $locations = get_nav_menu_locations();
                if ( isset( $locations[ 'mega_menu' ] ) ) {
                    $menu = get_term( $locations[ 'mega_menu' ], 'nav_menu' );
                    if ( $items = wp_get_nav_menu_items( $menu->name ) ) {
                            foreach ( $items as $item ) {  
                                echo "<li class=\"nav-item menu-item menu-item-type-post_type menu-item-object-page\">";
                                    echo "<a class=\"nav-link\" href=\"{$item->url}\">{$item->title}</a>";
                                        if ( is_active_sidebar( 'mega-menu-widget-area-' . $item->ID ) ) {
                                            echo "<div id=\"mega-menu-{$item->ID}\" class=\"mega-menu sub-nav\">";
                                                     dynamic_sidebar( 'mega-menu-widget-area-' . $item->ID );
                                            echo "</div>";
                                        }
                                echo "</li>";
                            }
                    }
                }
        ?>
    </ul>
</nav>