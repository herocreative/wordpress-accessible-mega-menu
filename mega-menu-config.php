<?php


/**
 * Mega Menu Config
 *
 * Place the below code in your functions file
 * The below scripts are needed to run properly
 * <script src="//code.jquery.com/jquery-1.10.1.min.js"></script>
 * <script src="js/jquery-accessibleMegaMenu.js"></script>
 * The Adobe Mega Menu Git Hub Page: https://github.com/adobe-accessibility/Accessible-Mega-Menu/. 
 */


// Registering the menu
function wpmm_setup() {
    register_nav_menus( array(
        'mega_menu' => 'Mega Menu'
    ) );
}
add_action( 'after_setup_theme', 'wpmm_setup' );


// Creates a widget area for any menu item in the mega menu that has a class 'has-mega'
function wpmm_init() {
    $location = 'mega_menu';
    $css_class = 'has-mega';
    $locations = get_nav_menu_locations();
    if ( isset( $locations[ $location ] ) ) {
        $menu = get_term( $locations[ $location ], 'nav_menu' );
        if ( $items = wp_get_nav_menu_items( $menu->name ) ) {
            foreach ( $items as $item ) {
                if ( in_array( $css_class, $item->classes ) ) {
                    $specialClass = get_field('class', 'widget_' . $widget_id);
                    register_sidebar( array(
                        'id'   => 'mega-menu-widget-area-' . $item->ID,
                        'name' => $item->title . ' - Mega Menu',
                        'before_widget' => '',
                        'after_widget' => '', 
                    ) );
                }
            }
        }
    }
}
add_action( 'widgets_init', 'wpmm_init' );


// Adds the ability to add a title with a link to each widget, this is optional. But it used it needs ACF to run properly, you must also create the field in ACF. You can also use another plugin but must change what the $title_with_link is equal to. 
add_filter('dynamic_sidebar_params', 'my_dynamic_sidebar_params');

function my_dynamic_sidebar_params( $params ) {
	

	$widget_name = $params[0]['widget_name'];
	$widget_id = $params[0]['widget_id'];
	

	if( $widget_name != 'Navigation Menu' ) {
		
		return $params;
		
	}
	
	
    // If using ACF replace 'your-field-title' with your ACF field name. 
	$title_with_link = get_field('your-field-title', 'widget_' . $widget_id);
	
	if( $title_with_link ) {
		
		$params[0]['before_widget'] .= '<div class="sub-nav-group">';
		$params[0]['before_widget'] .= '<h2 class="widgettitle">';
        $params[0]['before_widget'] .= '<a href="' . $title_with_link['url'] . '">' . $title_with_link['title'] . '</a>';
        $params[0]['before_widget'] .= '</h2>';
        $params[0]['after_widget'] .= '</div>';
       
		
		
	} else { 
    $params[0]['before_widget'] .= '<div class="sub-nav-group">';
         $params[0]['after_widget'] .= '</div>';
    
    }
	
	// return
	return $params;

}


// Creates intro widget, optional and also requires ACF to work properly
if(!class_exists('MegaIntroWidget')) {

  class MegaIntroWidget extends WP_Widget {

    /**
    * Sets up the widgets name etc
    */
    public function __construct() {
      $widget_ops = array(
        'classname' => 'mega_intro_widget',
        'description' => 'Intro for mega menus',
      );
      parent::__construct( 'mega_intro_widget', 'Mega Intro', $widget_ops );
    }

    /**
    * Outputs the content of the widget
    *
    * @param array $args
    * @param array $instance
    */
    public function widget( $args, $instance ) {
       // outputs the content of the widget
      if ( ! isset( $args['widget_id'] ) ) {
        $args['widget_id'] = $this->id;
      }

      // widget ID with prefix for use in ACF API functions
      $widget_id = 'widget_' . $args['widget_id'];

      // Fields for the widget, using ACF below.
      $btn_label = get_field( 'button_label', $widget_id );
      $btn_link = get_field( 'button_link', $widget_id ) ? get_field( 'button_link', $widget_id ) : '#';
      $text_field = get_field( 'text', $widget_id );

      echo $args['before_widget'];

      echo '<div class="mega-menu-intro"><div class="mega-menu-intro-inner">'; 
      
      echo '<p>' . $text_field . '</p>';

      if($btn_label) {
        echo '<a href="' . esc_url($btn_link) . '" class="button">' . esc_html($btn_label) . '</a>';
      }
        
        echo '</div></div>';

      echo $args['after_widget'];
    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form( $instance ) {
    	// outputs the options form on admin
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     *
     * @return array
     */
    public function update( $new_instance, $old_instance ) {
    	// processes widget options to be saved
    }

  }

}


// Register our CTA Widget

function register_mega_intro_widget()
{
  register_widget( 'MegaIntroWidget' );
}
add_action( 'widgets_init', 'register_mega_intro_widget' );



